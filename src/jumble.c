/* $Id: jumble.c 1.5 1998/12/29 01:16:18 brian Exp $ */
//=============================================================================
//
// Jumble : Solves a jumble of one or more words
//
// AUTHOR
//
//    Brian Yoder
//
// REMARKS
//
//    This program accepts one or more words from the command line. It
//    then prints all combinations of letters that are valid words using
//    the Dictionary array of character pointers to a sorted word list.
//
//    If more than one word is entered, then the lengths of the words are
//    remembered and the letters combined together. Then the program
//    prints all possible letter combinations that form valid words whose
//    word count and lengths are the same as the originally entered words.
//
// CHANGE HISTORY
//
//    27 Dec 1998 : Created
//    28 Dec 1998 : Remove duplicate matches.
//
//=============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <sys/types.h>

typedef unsigned char   uchar;
typedef unsigned short  ushort;
typedef unsigned int    uint;
typedef unsigned long   ulong;

//-----------------------------------------------------------------------------
//
// These must be defined in the words.c module. A dictionary file (i.e. a
// sorted lower-case word list file) may be created using the words2c Perl
// script as follows:
//
//    perl words2c words.lst Dictionary >words.c
//
//-----------------------------------------------------------------------------

extern char   *Dictionary[];
extern int     DictionaryLength;

//-----------------------------------------------------------------------------
//
// Some global definitions
//
//-----------------------------------------------------------------------------

enum {
  MaxWords = 4,                       // Max no. of words on the command line

  MaxLen   = 32,                      // Max. no. of letters, total and per-word
  BufLen   = MaxLen+1,                // Buffer length: include null terminator

  MaxMatch = 512                      // Max. no. of matches that are supported
};

//-----------------------------------------------------------------------------
//
// Structure and data that define the lengths of the original words that
// were entered on the command line.
//
//-----------------------------------------------------------------------------

typedef struct {

  uint     wordLen;                   // Length of this word

} LookUpInfo;

static uint       gWordCnt;
static LookUpInfo gWordInfo[MaxWords];

//-----------------------------------------------------------------------------
//
// Structure and data for the matches that we have found so far. The
// SaveMatch() function adds unique matches to the gMatch[] array and
// ignores duplicates.
//
//-----------------------------------------------------------------------------

static uint       gMatchCnt;
static char     **gMatchNext;
static char      *gMatch[MaxMatch];
static char       gMatchBuf[BufLen * MaxMatch];

//-----------------------------------------------------------------------------
//
// Prototypes for functions private to this source module
//
//-----------------------------------------------------------------------------

static void StrLower  ( char * str );

static void SortWord  ( char * word );
static int  compChar  ( const void *, const void * );

static void ShowComb  ( char *pfx, char *word );
static void ShowAll   ( char *word );

static int  SaveMatch ( char *word );
static int  FindMatch ( char *word );
static int  compWord  ( const void *, const void * );

static int  LookUpAll ( char *dictionary[], uint dictionaryLen, char *word );
static int  LookUp    ( char *dictionary[], uint dictionaryLen, char *word );
static int  iskeyWord ( const void *p1, const void *p2 );

//=============================================================================
//
// MAIN PROGRAM ENTRY POINT
//
//=============================================================================

int main ( int argc, char **argv )
{
  uint        wordIx;
  uint        wordLen = 0;
  char        word[BufLen];

  uint        i;
  char       *s;

  LookUpInfo *info;

  argc--; argv++;
  if ((argc < 1) || (argc > MaxWords))
  {
    fprintf(stderr, "USAGE: jumble word [ word2 [ word3 [ word4] ] ]\n");
    return 2;
  }

  // Initialize the array of pointers to stored matches

  gMatchCnt = 0;
  gMatchNext = gMatch;

  for (i=0, s=gMatchBuf; i<MaxMatch; i++, s+=BufLen)
  {
    gMatch[i] = s;
  }

  // Combine all of the words entered on the command line into the word[]
  // buffer. Also initialize the individual word's lookup information.

  memset(word, 0, sizeof(word));
  for (wordIx=0, info=gWordInfo; wordIx<argc; wordIx++, info++, argv++)
  {
    wordLen += strlen(*argv);
    if (wordLen > MaxLen)
    {
      fprintf(stderr, "Length of all words must not exceed %u characters\n", MaxLen);
      return 2;
    }

    strcat(word, *argv);
    info->wordLen = strlen(*argv);

    gWordCnt++;
  }

  // For all possible combinations of all letters, lookup the word(s) in
  // the Dictionary array

  StrLower(word);
  SortWord(word);
  ShowComb("", word);

  return 0;
}

//=============================================================================
//
// SortWord() : Sorts the letters of a word in-place
//
//=============================================================================

static void SortWord ( char *word )
{
  qsort((void *)word, (uint)strlen(word), sizeof(char), compChar);
}

static int  compChar ( const void *c1, const void *c2 )
{
  return *((char *)c1) > *((char *)c2);
}

//=============================================================================
//
// StrLower() : Converts string to lower case in place.
//
//=============================================================================

static void StrLower ( char * str )
{
  if (str != NULL)
  {
    char *p;
    for ( p = str; *p != '\0'; p++)
    {
      *p = (char)(tolower(*p));
    }
  }
}

//=============================================================================
//
// ShowComb() : Shows/processes word combinations
//
// REMARKS
//
//    This function processes all possible combinations of the letters in
//    the specified word with the specified prefix added to the front of
//    each combination.
//
//    You normally call it with an empty string for the prefix. ShowComb()
//    then calls itself recursively as needed to print all of the
//    combinations.
//
// RETURNS
//
//    Nothing.
//
//=============================================================================

static void ShowComb ( char * pfx, char * word )
{
  char  myPfx[BufLen];
  char  myWord[BufLen];
  char *endPfx;
  char *endWord;

  uint  pfxLen  = strlen(pfx);
  uint  wordLen = strlen(word);

  uint  i, j;
  char  c;

  if (wordLen == 1)
  {
    //-----------------------------------------------------------------------
    //
    // Word length is 1: There is only one possible combination of the
    // word
    //
    //-----------------------------------------------------------------------
    strcpy(myWord, pfx);
    strcat(myWord, word);
    if (LookUpAll(Dictionary, DictionaryLength, myWord) > 0) ShowAll(myWord);
    return;
  }
  else
  {
    //-----------------------------------------------------------------------
    //
    // Word length is 2 or more: For each letter in the word, show
    // combinations of the current prefix followed by one of the letters
    // in the word followed by the rest of the letters in the word. i is
    // used to step through each letter of the word, and j is used to
    // step through each letter index to determine if the letter at
    // index i should be added to the prefix or to the new word.
    // Therefore, the prefix will be one letter longer than the current
    // prefix and the new word will be one letter shorter than the
    // current word.
    //
    // Here's where the recursion works its magic. Starting with a word
    // of N letters (where N is 2 or more, of course), we show the N
    // combinations that consist of the current prefix followed by one
    // of the N letters followed by the rest of the letters (all except
    // the one added to the prefix). Each time we call ourselves, the
    // prefix grows by one letter and the word shrinks by one letter.
    // Once we reach the case above where the word is only one letter
    // long, we are ready to process the prefix followed by the word.
    //
    //-----------------------------------------------------------------------
    for (i=0; i<wordLen; i++)
    {
      strcpy(myPfx, pfx);
      endPfx = &myPfx[pfxLen];
      endWord = myWord;

      for (j=0; j<wordLen; j++)
      {
        c = word[j];
        if (j == i) *endPfx++ = c;
        else        *endWord++ = c;
      }

      *endPfx++ = '\0';
      *endWord++ = '\0';
      ShowComb(myPfx, myWord);
    }

    return;
  }
}

//=============================================================================
//
// ShowAll() : Shows all individual words based on the words entered
//
// REMARKS
//
//    The global gWordCount and gWordInfio[] variables specify just how
//    many words were originally entered on the command line and the
//    lengths of those words.
//
//=============================================================================

static void ShowAll ( char *word)
{
  uint        i;
  LookUpInfo *winfo;
  char       *w;
  uint        len;
  char        c;

  if (!SaveMatch(word)) return;

  if (gWordCnt == 1)
  {
    puts(word);
  }
  else
  {
    // For each possible word that was originally entered:

    for (i=0, winfo=gWordInfo, w=word; i<gWordCnt; i++, winfo++)
    {
      if (i>0) printf(" ");

      // Save the character just past it and then replace it with a
      // null terminator and show it

      len = winfo->wordLen;
      c = w[len];
      w[len] = '\0';
      printf("%s", w);

      // Replace the null terminator with the character that was saved,
      // which is the first character of the next word

      w[len] = c;
      w += len;
    }
    printf("\n");
  }
}

//=============================================================================
//
// SaveMatch() : Saves unique matched word
//
// REMARKS
//
//    This function assumes that the character pointers in the gMatch[]
//    array are sorted. If the word is not already in the gMatchBuf[]
//    buffer (and its pointer not in the gMatch[] array), then the word is
//    stored and the array is resorted.
//
// RETURNS
//
//    0 : The word is a duplicate of a word that is already stored.
//    1 : The word was stored successfully.
//
//=============================================================================

static int SaveMatch( char *word )
{
  static uint gErrCnt = 0;

  char    *w;

  if (FindMatch(word)) return 0;

  if (gMatchCnt >= MaxMatch)
  {
    if (gErrCnt == 0)
    {
      gErrCnt++;
      fflush(stdout);
      fprintf(stderr, "Exceeded number of unique matches (%u)\n", (uint)MaxMatch);
      fflush(stderr);
    }
    return 1;
  }

  w = *gMatchNext++;
  strcpy(w, word);
  gMatchCnt++;

  qsort(gMatch, gMatchCnt, sizeof(char *), compWord);

  return 1;
}

static int  FindMatch ( char *word )
{
  void    *p;

  p = bsearch ( word, gMatch,
                (size_t)gMatchCnt,
                (size_t)sizeof(char *),
                iskeyWord );

  if (p == 0) return 0;
  return 1;
}

static int  compWord ( const void *p1, const void *p2 )
{
  return strcmp( *(char **)p1, *(char **)p2 );
}

//=============================================================================
//
// LookUpAll() : Looks up all individual words
//
// REMARKS:
//
//    The global gWordCount and gWordInfio[] variables specify just how
//    many words were originally entered on the command line and the
//    lengths of those words.
//
//    If there was only one word being looked up, then just look it up.
//    However, if the word being passed in is made up of more than one
//    individual word, then look up each of these individual words on its
//    own. If they all match, then return true.
//
// RETURNS: 0 (false) if some words aren't valid, or 1 (true) if all are.
//
//=============================================================================

static int  LookUpAll ( char *dictionary[], uint dictionaryLen, char *word )
{
  uint        i;
  LookUpInfo *winfo;
  char       *w;
  uint        len;
  char        c;

  if (gWordCnt == 1)
  {
    return LookUp(dictionary, dictionaryLen, word);
  }
  else
  {
    // For each possible word that was originally entered:

    for (i=0, winfo=gWordInfo, w=word; i<gWordCnt; i++, winfo++)
    {
      // Save the character just past it and then replace it with a
      // null terminator for the comparison/lookup function.

      len = winfo->wordLen;
      c = w[len];
      w[len] = '\0';
      //printf(" %s", w);

      // If we don't find this individual word, then the entire phrase
      // must be rejected

      if (LookUp(dictionary, dictionaryLen, w) == 0) return 0;

      // Replace the null terminator with the character that was saved
      // (which is the first character of the next word)

      w[len] = c;
      w += len;
    }
    //printf("\n");
  }

  return 1;
}

//=============================================================================
//
// LookUp() : Looks up a word in a dictionary
//
// RETURNS: 0 (false) if word isn't valid, or 1 (true) if it is.
//
//=============================================================================

int LookUp ( char *dictionary[], uint dictionaryLen, char *word)
{
  void    *p;

  p = bsearch ( word, dictionary,
                (size_t)dictionaryLen,
                (size_t)sizeof(char *),
                iskeyWord );

  if (p == 0) return 0;
  return 1;
}

static int  iskeyWord ( const void *p1, const void *p2 )
{
  //printf("  strcmp(%s, %s)\n", (char *)p1, *(char **)p2);
  return strcmp( (char *)p1, *(char **)p2 );
}
