README

        JUMBLE - A program to solve scrambled English words or phrases

AUTHOR

        Brian Yoder

DATE

        1998/12/29 02:09:08

WHAT IT IS

        Jumble accepts from one to four scrambled words on the command
        line. Jumble calculates all possible combinations of letters and
        prints out each combination that are entirely valid English
        words. Letters may be entered in lowercase or UPPERCASE or mIxEd
        case: it doesn't matter.

        If more than one scrambled word is entered, then the lengths of
        the scrambled words are identical to the lengths of the final
        unscrambled words.

        This follows the pattern of the typical Jumble puzzle. There are
        a set of scrambled words and there is a final phrase. The final
        phrase may consist of one or more words and the lengths of the
        words are predetermined by the puzzle.

        Modern CPUs can handle up to 11 letters before it starts to bog
        down. 12 takes a while but is feasible. More than 12 and things
        really take a very long time to solve.

FOR EXAMPLE

        Here is an example of a Jumble puzzle that contained four
        scrambled words and a final three-word phrase. In each of the
        four words, an underscore indicates a letter position and an O
        indicates a letter position that is also a letter in the final
        phrase:

             CHEATTER : _ O _ _ _ _ _ _

             SHELPESL : _ _ _ _ _ _ _ O

             TREAUREC : O O _ O _ _ _ _

             FAZILINE : _ _ _ _ _ O O O


             Phrase:    _ _    _   _ _ _ _ _

        Here are the commands used to solve the first four words:

             $ jumble cheatter
             $ jumble shelpesl
             $ jumble treaurec
             $ jumble faziline

        Once the words are unscrambled and the circled letters are listed, the
        following letters are found: ASCRAIZE

        Split the letters into groups whose lengths are the same as the
        lengths of the words in the phrase: 2 letters, then a space,
        then 1 letter, then a space, and finally 5 letters. It doesn't
        match in which order the letters are listed. But it does matter
        that the word lengths are correct. Here is the command that is
        used to solve the phrase:

             $ jumble as c raize

        Jumble lists eight possible phrases. Only one, IS A CRAZE, is
        actually correct. In this case, the puzzle's cartoon made it
        easy to select the correct phrase from the list of possible
        phrases.

HOW IT WORKS (IF YOU DON'T CARE, SKIP THIS PART!)

        Jumble is quite simplistic but it works.

        An English dictionary with 109,582 words is embedded into
        Jumble. This dictionary is freely available from the Internet as
        the words.lst file. The words2c program, a Perl 5 script, is
        used to convert the words.lst file into a C language array as
        follows:

                perl words2c words.lst Dictionary >words.c

        The words.c and jumble.c files are then compiled and linked to
        form the jumble.exe program.

        Jumble remembers how many words you entered and then combines
        them into a single large word. Jumble uses a recursive algorithm
        to create a list of all possible letter combinations in this
        word. Note that if you entered a total of N letters, there are
        N! (N factorial) combinations. Jumble is quick up to about 11
        letters, not too bad for 12 letters, and very slow for 13 or
        more letters.

        Jumble looks up each and every one of those letter combinations
        in its dictionary. If you entered more than one scrambled word,
        Jumble breaks up each and every one of these combinations into
        groups of the correct lengths and looks up each one of these
        groups. As you can see, Jumble isn't intuitive in the slightest
        and uses brute force to try all possible combinations.

        Jumble uses a binary search algorithm to look up a group of
        letters in the dictionary to find out whether or not those
        letters form a word. Of course, there are much, much faster
        spell checking algorithms that can be used or developed to work
        with a dictionary. Since a dictionary is only loaded and
        processed once and is not updated while Jumble is running, it
        can be built in a way that makes searches faster at the expense
        of slow and complicated insertions. But as you have guessed by
        now, I didn't use one of those algorithms.

        Jumble fails to find a resulting phrase if one or more words in
        that phrase are not real words but sound-alike puns that are
        (typically) composed of one real word and a suffix of some sort,
        often connected by a dash. But these are relatively rare.

END OF FILE
